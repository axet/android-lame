# liblame for Android

Library to encode samples to mp3.

# Natives


```java
    import com.github.axet.androidlibrary.app.Natives;

    Native.loadLibraries(context, new String[]{"lame", "lamejni"})
```

```gradle
dependencies {
    compile 'com.github.axet:lame:1.0.9'
}
```

# Compile

Install compile deps:

    # apt install libncurses5

Then compile the project:

    # gradle clean
    # gradle :liblame:publishToMavenLocal publishToMavenLocal
    # gradle :liblame:publish publish
